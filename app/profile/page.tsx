import ProfileContent from '@/components/profile/ProfileContent';
import { FC } from 'react';

interface pageProps {

}

const page: FC<pageProps> = ({ }) => {
    return (
        <div>
            <ProfileContent />
        </div>
    )
}

export default page;