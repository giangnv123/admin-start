export const domainFileServer = `http://localhost:8088`;
// export const domainFileServer = `https://drive.xpi.vn`; 

export const domainWorkerService = `http://localhost:8080`;

export const API_FILES = `${domainFileServer}/xapi/file`;
export const API_FILES_MULTIPLE = `${domainFileServer}/xapi/file/multiple`;

