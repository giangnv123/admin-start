import FileContent from "@/components/dashboard/FileContent";
import { FC } from "react";

interface pageProps {
}

const Page: FC<pageProps> = () => {

  return (
    <div className="">
      <FileContent />
    </div>
  );
};

export default Page;
