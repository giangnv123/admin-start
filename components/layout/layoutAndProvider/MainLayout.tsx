"use client"
import { FC, ReactNode, useState } from 'react';
import { Toaster } from 'sonner';

import { IUser } from '../../base/props/interface';
import Header from '../header/Header';
import Navbar from '../navbar/Navbar';


interface MainLayoutProps {
    children: ReactNode;
    user: IUser
}

const MainLayout: FC<MainLayoutProps> = ({ children, user }) => {

    const [showNav, setShowNav] = useState<boolean>(false);
    const toggleNav = () => setShowNav(!showNav);

    return (
        <main >
            <Toaster position="top-center" richColors />
            <Header toggleNav={toggleNav} />
            <div className='flex'>
                <Navbar showNav={showNav} toggleNav={toggleNav} user={user as IUser} />
                <div className='lg:pl-72 flex-1  min-h-[calc(100vh-64px)] '>
                    <div className='p-4 bg-gray-50'>
                        {children}
                    </div>
                </div>
            </div>
        </main>
    );
}

export default MainLayout;
