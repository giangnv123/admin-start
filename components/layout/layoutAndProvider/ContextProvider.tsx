'use client'
import { FC, ReactNode, useState } from 'react';
import { GlobalContext } from '../../base/context/GlobalContext';
import Provider from './Provider';

interface ContextProviderProps {
    children: ReactNode
}

const ContextProvider: FC<ContextProviderProps> = ({ children }) => {
    const [count, setCount] = useState<number>(0);
    const increCount = () => setCount(prev => prev + 1);
    return (
        <GlobalContext.Provider value={{ count, increCount }}>
            <Provider>
                {children}
            </Provider>
        </GlobalContext.Provider >
    )
}

export default ContextProvider;