"use client"
import { QueryClient } from '@tanstack/react-query';
import { useSession } from 'next-auth/react';
import { FC, ReactNode } from 'react';

import Loading from '@/components/base/shared/Loading';
import SignInUi from '@/components/base/signIn/SignInUi';
import { IUser } from '../../base/props/interface';
import MainLayout from './MainLayout';

const queryClient = new QueryClient()

interface LayoutAuthProps {
    children: ReactNode;
}

const LayoutAuth: FC<LayoutAuthProps> = ({ children }) => {
    const { data: session, status } = useSession();

    // Handling the loading state
    if (status === "loading") {
        return (
            <div className='h-screen flex justify-center items-center'>
                <Loading />
            </div>
        )
    }

    if (status === "unauthenticated") {
        return (
            <SignInUi />
        );
    }

    return (
        <MainLayout user={session?.user as IUser}>
            {children}
        </MainLayout>
    );
}

export default LayoutAuth;
