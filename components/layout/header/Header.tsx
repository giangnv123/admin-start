import { Button } from "@/components/ui/button";
import { cn } from "@/lib/utils";
import { LogOutIcon, MenuIcon, User2Icon } from "lucide-react";
import { signOut } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
import { memo } from "react";
import UserHoverCard from "./UserHoverCard";


const Header = ({ toggleNav }: { toggleNav: () => void }) => {
    return (
        <div className={cn(`h-16 w-full sticky top-0 z-50 shadow-md bg-green-600 
        border-b border-slate-300  
        flex items-center justify-between p-1.5 px-5 sm:px-10`)}>
            <Image color="red" width={150} height={150} src={"/logo.svg"} alt="logo" />
            <button className="lg:hidden  text-white" onClick={toggleNav}>
                <MenuIcon />
            </button>

            <div className="flex items-center gap-4 ">
                <UserHoverCard className="w-40 rounded-md p-5"
                    content={
                        <div className="space-y-3">
                            <button
                                className='text-gray-500 font-semibold text-sm flex items-center gap-2 hover:text-green-600'
                                onClick={() => signOut()}><LogOutIcon width={20} height={20} />Đăng xuất</button>
                            <Link className="text-gray-500 font-semibold text-sm flex items-center gap-2 hover:text-green-600"
                                href={"/profile"}><User2Icon /> Thông tin</Link>
                        </div>
                    }>
                    <button className='text-white font-semibold text-sm flex items-center gap-2'><User2Icon /> Tài khoản</button>
                </UserHoverCard>
                <Button className="ml-4" variant="outline">
                    Đăng tin
                </Button>
            </div>
        </div>
    )
}

export default memo(Header);