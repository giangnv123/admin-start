import { memo } from "react"

import {
    HoverCard,
    HoverCardContent,
    HoverCardTrigger,
} from "@/components/ui/hover-card"
import { cn } from "@/lib/utils"


interface HoverCardProps {
    children: React.ReactNode,
    content: React.ReactNode,
    className?: string
}

const UserHoverCard = ({ children, content, className }: HoverCardProps) => {
    return (
        <HoverCard openDelay={200}>
            <HoverCardTrigger asChild>
                {children}
            </HoverCardTrigger>
            <HoverCardContent className={cn("w-24", className)}>
                {content}
            </HoverCardContent>
        </HoverCard>
    )
}

export default memo(UserHoverCard);
