import { cn } from '@/lib/utils';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { FC, memo } from 'react';


interface NavItemProps {
    item: navItemProps
}

export interface navItemProps {
    name: string;
    path: string;
    icon: React.ReactNode
}

const NavItem: FC<NavItemProps> = ({ item }) => {
    const pathname = usePathname()

    return (
        <li key={item.name}>
            <Link
                className={cn(`py-3 flex items-center gap-4 px-6  text-gray-600 
                            hover:bg-gray-100 font-semibold rounded-r-full`,
                    { "bg-blue-50 hover:bg-blue-50 text-blue-500 font-semibold": pathname === item.path })}
                href={item.path}>{item.icon} {item.name}</Link>
        </li>
    )
}

export default memo(NavItem);