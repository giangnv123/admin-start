'use client'

import { IUser } from '@/components/base/props/interface';
import Avatar from '@/components/base/shared/Avatar';
import { cn } from '@/lib/utils';
import { FC } from 'react';
import NavItem from './NavItem';
import { NavListContent } from './NavListContent';

interface NavbarProps {
    user: IUser;
    showNav: boolean;
    toggleNav: () => void
}

const Navbar: FC<NavbarProps> = ({ user, showNav }) => {

    return (
        <div className={cn(`fixed -left-full h-screen overflow-y-scroll top-16 lg:left-0 transition-all bg-white py-5 w-72 
        border-r border-gray-200 overflow-hidden`,
            { "left-0": showNav, })}>

            {/* Avatar */}
            <div className='px-4'>
                <Avatar user={user} />
            </div>

            {/* Content */}
            <ul className='pr-4 space-y-1'>
                {NavListContent.map(item =>
                    <NavItem item={item} key={item.name} />)}
            </ul>
        </div>
    )
}


export default Navbar;