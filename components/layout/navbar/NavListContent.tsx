import { ImagesIcon, NewspaperIcon, RectangleEllipsis, User2Icon } from 'lucide-react';
import { navItemProps } from './NavItem';


export const NavListContent: navItemProps[] = [
    {
        name: 'Thư mục',
        path: '/',
        icon: <ImagesIcon />
    },
    {
        name: 'Tài khoản',
        path: '/profile',
        icon: <User2Icon />
    },
    {
        name: 'Bài viết',
        path: '/post',
        icon: <NewspaperIcon />
    },
    {
        name: 'Chờ phê duyệt',
        path: '/pending',
        icon: <RectangleEllipsis />
    },


]