"use client"

import { createContext, useContext } from "react";

export const GlobalContext = createContext<GlobalContextProps | undefined>(undefined);

export const GetGlobalContext = () => {
    const global = useContext(GlobalContext)
    if (global === undefined) {
        throw new Error("useGlobalContext must be used within a GlobalProvider")
    }
    return global
}

export interface GlobalContextProps {
    count: number;
    increCount: () => void;
}

