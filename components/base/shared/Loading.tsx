import { FC } from 'react';

interface LoadingProps {

}

const Loading: FC<LoadingProps> = ({ }) => {
    return (
        <div>
            <div className="lds-hourglass"></div>
        </div>
    )
}

export default Loading;