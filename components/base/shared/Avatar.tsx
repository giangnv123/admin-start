import { cn } from '@/lib/utils';
import Image from 'next/image';
import { FC, memo } from 'react';
import { IUser } from '../props/interface';

interface AvatarProps {
    user: IUser;
    showName?: boolean;
    className?: string;
    width?: number;
    height?: number;
}

const Avatar: FC<AvatarProps> = ({ user, showName = true, className, width = 50, height = 50 }) => {
    return (
        <div className={cn('flex flex-wrap items-center gap-2 mb-4 text-gray-500 font-semibold', className)}>
            <Image alt='user' width={width} height={height} className='rounded-full shadow-md object-cover' src={user.image} />
            {showName && <h3 >{user.name}</h3>}
        </div>
    )
}

export default memo(Avatar);