'use client'
import { FC } from 'react';
import { GetGlobalContext } from '../base/context/GlobalContext';

interface FileContentProps {

}

const FileContent: FC<FileContentProps> = ({ }) => {
    const globalContext = GetGlobalContext();

    return (
        <div>
            <h2> Count value: {globalContext.count}</h2>
        </div>
    )
}

export default FileContent;